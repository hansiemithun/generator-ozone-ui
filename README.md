# generator-ozone-ui [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url] [![Coverage percentage][coveralls-image]][coveralls-url]

> Create react app with extra features for rapid development

## Preview

![Production](//i.imgur.com/Vcn1YxU.png)

## Installation

First, install [Yeoman](http://yeoman.io) and generator-ozone-ui using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g generator-ozone-ui
```

Then generate your new project:

Create a directory **Ex:** `foo` and get inside that `foo/` directory

**_Run:_**

```bash
yo ozone-ui
```

## Getting To Know Yeoman

- Yeoman has a heart of gold.
- Yeoman is a person with feelings and opinions, but is very easy to work with.
- Yeoman can be too opinionated at times but is easily convinced not to be.
- Feel free to [learn more about Yeoman](http://yeoman.io/).

## Features

- Precommit hooks enabled for clean code
- Istanbul Coverage is reported
- Mock server configured
- Local application can target to multiple servers

## Additional Dependencies

Most commonly used dependencies are included in this package.

`Flexbox-Grid` `Axios` `Redux` `React Hooks` `Fakers` `React Dev Tools` `Cors` `Helmet` `React-Router`

## After Installation

In the project directory, you can run:

### `npm start:dev`

Runs the app in the development mode.

API_URL: [http://localhost:4001](http://localhost:4001)
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.

You will also see any lint errors in the console.

### `npm start:staging`

Runs the app in the development mode.

API_URL: [https://ozone-staging.herokuapp.com](https://ozone-staging.herokuapp.com)
Open [http://localhost:3000](http://localhost:3002) to view it in the browser.

The page will reload if you make edits.

You will also see any lint errors in the console.

### `npm start:prod`

Runs the app in the development mode.

API_URL: [https://ozone-production.herokuapp.com](https://ozone-production.herokuapp.com)
Open [http://localhost:3000](http://localhost:3003) to view it in the browser.

For more information about customisation on this, refer: [https://bitbucket.org/hansiemithun/generator-ozone-ui/src/master/generators/app/templates/ozone-ui/README.md](https://bitbucket.org/hansiemithun/generator-ozone-ui/src/master/generators/app/templates/ozone-ui/README.md)

## License

MIT © [Mithun H A](https://www.npmjs.com/~hansiemithun)

[npm-image]: https://badge.fury.io/js/generator-ozone-ui.svg
[npm-url]: https://npmjs.org/package/generator-ozone-ui
[travis-image]: https://travis-ci.org/hansiemithun/generator-ozone-ui.svg?branch=master
[travis-url]: https://travis-ci.org/hansiemithun/generator-ozone-ui
[daviddm-image]: https://david-dm.org/hansiemithun/generator-ozone-ui.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/hansiemithun/generator-ozone-ui
[coveralls-image]: https://coveralls.io/repos/hansiemithun/generator-ozone-ui/badge.svg
[coveralls-url]: https://coveralls.io/r/hansiemithun/generator-ozone-ui
