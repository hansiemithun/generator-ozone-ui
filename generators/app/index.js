'use strict';
const Generator = require('yeoman-generator');
const chalk = require('chalk');
const yosay = require('yosay');
const mkdirp = require('mkdirp');

module.exports = class extends Generator {
  constructor(args, opts) {
    super(args, opts);
    this.tmpDir = 'ozone-ui';
  }

  prompting() {
    this.log(
      yosay(
        `Welcome to the divine ${chalk.red('generator-ozone-ui')} generator!`
      )
    );

    const prompts = [
      {
        type: 'input',
        name: 'projectName',
        message: 'Your project name',
        default: this.appname.replace(/\s+/g, '-')
      }
    ];

    this.log(this.appname.replace(/\s+/g, '-') + ' is getting generated..!');

    return this.prompt(prompts).then(props => {
      this.props = props;
    });
  }

  writing() {
    this.fs.copy(
      this.templatePath(this.tmpDir + '/'),
      this.destinationPath(this.destinationRoot()),
      { globOptions: { dot: true } }
    );

    this.fs.copyTpl(
      this.templatePath(this.tmpDir + '/_package.json'),
      this.destinationPath('package.json'),
      {
        name: this.props.projectName.replace(/\s+/g, '-')
      }
    );

    this.fs.copy(
      this.templatePath(this.tmpDir + '/_gitignore'),
      this.destinationPath('.gitignore')
    );

    this.removeFiles();
  }

  removeFiles() {
    this.fs.delete(this.destinationRoot() + '/_package.json');
    this.fs.delete(this.destinationRoot() + '/_gitignore');
  }

  install() {
    this.installDependencies({
      bower: false
    });
  }
};
